<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TodoController;
use App\Http\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

/**
 * public routes
 * 
 */

Route::post('/login', [AuthController::class, 'login']);
Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
Route::post('/forgot-password', [AuthController::class, 'forgot_password']);
Route::post('/reset-user-password', [AuthController::class, 'reset_password']);
Route::apiResource('todos',TodoController::class);

// Route::middleware(['auth:sanctum'])->group(function () {

//     // Route::apiResource('/products', ProductController::class);
//     // Route::get('/products',[ProductController::class, 'index']);
//     // Route::post('/products',[ProductController::class , 'store']);
//     // Route::put('/products/{product}',[ProductController::class , 'update']);
//     // Route::delete('/products',[ProductController::class , 'delete']);

   



// });

