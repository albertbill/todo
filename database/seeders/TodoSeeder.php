<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Todo;

class TodoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $todos = [
            [
                'name' => 'Talk To Jonte',
            ],
            [
                'name' => 'Ask a question',
            ],
            [
                'name' => 'To the Todo list',
            ],
            [
                'name' => 'Submit dummy project',
            ],

            [
                'name' => 'Get roasted',
            ],
        ];

        foreach ($todos as $todo) {
            Todo::create($todo);
        }
    }
}