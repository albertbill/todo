<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        if(Product::count() > 0){
            return;
        }

        $data = [
            [
                'title' => 'Product one',
                'body' => 'This is the first product on the list',
                'user_id' => 1 ,
            ],
            [
                'title' => 'Product Two',
                'body' => 'This is the seconde product 0n the list',
                'user_id' => 1 ,
            ],
            [
                'title' => 'Product Three',
                'body' => 'This is the third product on the list',
                'user_id' => 1 ,
            ],
            [
                'title' => 'Product Four',
                'body' => 'This is the fourth product on the list',
                'user_id' => 1 ,
            ]
        ];


        Product::insert($data);

    }
}
