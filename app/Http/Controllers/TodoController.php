<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Todo;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Todo::all();
        return response([
            'message' => 'Todos out',
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only('name');
        $data = Todo::create([
            'name' => $data['name']
        ]);

        return response([
            'message' => 'Todo created successfully',
            'data' => $data
        ], 200);


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = Todo::findOrFail($id);
        return response([
            'message' => 'Todo found!',
            'data' => $data
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data = Todo::findOrFail($id);

        $req = $request->only('name');

        $data = $data->update([
            'name' => $req['name']
        ]);

        return response([
            'message' => 'Todo updated successfully',
            'data' => $data
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $data = Todo::findOrFail($id);

        $data = $data->delete();

        return response([
            'message' => 'Todo updated successfully',
            'data' => $data
        ], 200);
    }
}