<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Events\ProductUpdated;
use App\Http\Traits\DataTrait;
use Illuminate\Http\Request;


use App\Models\Product;


class ProductController extends Controller
{

    use DataTrait;
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $data =  $this->getData(new Product);


        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        return 'Got to store';
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductRequest $request,Product $product)
    {

        // return 'Got to update';
        // dump('Dumping this out');


        $product->update($request->validated());

        ProductUpdated::dispatch($product);

        return $product;
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
