<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {

            $rules = [
                'email' => 'required|email|exists:users,email',
                'password' => 'required|string'
            ];


            $validator = Validator::make($request->all(), $rules);

            if ($validator->fails()) {
                $data = [
                    'success' => false,
                    'message' => $validator->errors()->first()
                ];

                return response()->json($data, 422);
            }


            $user = User::where('email', $request->email)->first();

            //check password
            if (!Hash::check($request->password, $user->password)) {

                return response([
                    'message' => 'Incorrect password.Try again',
                ], 200);
            }

            //create token
            $token = $user->createToken('my-app-token')->plainTextToken;

            $response = [
                'success' => true,
                'message' => 'User login successfully',
                'token' => [
                    'access_token' => $token,
                    'token_type' => 'Bearer',
                ],
                'user' => $user
            ];


            return response()->json($response, 200);

        } catch (\Throwable $th) {
            throw $th;
        }

    }


    public function logout(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();

            return response([
                'success'=>true,
                'message' => 'Log out successful',
            ], 200);

        } catch (\Throwable $th) {
            throw $th;
        }

    }

    public function forgot_password(Request $request)
    {
        return 'Got to forgot password';
    }

    public function reset_password(Request $request)
    {
        return 'Got to reset password';
    }
}